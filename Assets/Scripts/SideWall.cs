using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SideWall : MonoBehaviour
{
   [SerializeField] private PlayerControl player;
   [SerializeField] private GameManager gameManager;

   private void OnTriggerEnter2D(Collider2D other)
   {
      if (other.gameObject.name != "Ball") return;
      player.IncrementScore();

      if (player.Score < gameManager.MAXScore)
      {
         other.gameObject.SendMessage("RestartGame", 2f, SendMessageOptions.RequireReceiver);
      }

   }
}
