using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControl : MonoBehaviour
{
    [SerializeField] private KeyCode upButton = KeyCode.W;
    [SerializeField] private KeyCode downButton = KeyCode.S;
    [SerializeField] private float speed = 10f;
    [SerializeField] private float yBoundary = 9f;

    private Rigidbody2D m_rigidbody;

    private ContactPoint2D m_lastContactPoint;

    public int Score { get; private set; }

    public ContactPoint2D LastContactPoint => m_lastContactPoint;

    private void Awake()
    {
        m_rigidbody = GetComponent<Rigidbody2D>();
    }

    private void Update()
    {
        MoveRacket();

        WrapScene();
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.name.Equals("Ball"))
        {
            m_lastContactPoint = other.GetContact(0);
        }
    }

    private void WrapScene()
    {
        var position = transform.position;
        if (position.y > yBoundary)
        {
            position.y = yBoundary;
        }
        else if (position.y < -yBoundary)
        {
            position.y = -yBoundary;
        }

        transform.position = position;
    }

    private void MoveRacket()
    {
        var velocity = m_rigidbody.velocity;
        if (Input.GetKey(upButton))
        {
            velocity.y = speed;
        }
        else if (Input.GetKey(downButton))
        {
            velocity.y = -speed;
        }
        else
        {
            velocity.y = 0f;
        }

        m_rigidbody.velocity = velocity;
    }

    public void IncrementScore()
    {
        Score++;
    }

    public void ResetScore()
    {
        Score = 0;
    }
    
}
