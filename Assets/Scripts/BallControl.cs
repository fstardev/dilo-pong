using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class BallControl : MonoBehaviour
{
   [SerializeField] private float maxForce;
   [SerializeField] private float yInitialForce;

   private Rigidbody2D m_rigidbody;


   public Vector2 TrajectoryOrigin { get; private set; }

   private void Awake()
   {
      m_rigidbody = GetComponent<Rigidbody2D>();
   }

   private void Start()
   {
      RestartGame();
      TrajectoryOrigin = transform.position;
   }

   private void OnCollisionExit2D(Collision2D other)
   {
      TrajectoryOrigin = transform.position;
   }

   private void ResetBall()
   {
      transform.position = Vector2.zero;
      m_rigidbody.velocity = Vector2.zero;
   }

   private void PushBall()
   {
   
      var randomDirection = Random.Range(0, 2);
      var yRandomInitialForce = Random.Range(-yInitialForce, yInitialForce);
      var xInitialForce = Mathf.Sqrt((maxForce * maxForce) - (yRandomInitialForce * yRandomInitialForce));
      
      m_rigidbody.AddForce(randomDirection < 1f
         ? new Vector2(-xInitialForce, yRandomInitialForce)
         : new Vector2(xInitialForce, yRandomInitialForce));

   }

   private void RestartGame()
   {
      StartCoroutine(RestartGameRoutine());
   }

   private IEnumerator RestartGameRoutine()
   {
      ResetBall();
      yield return new WaitForSeconds(2f);
      PushBall();
   }
}
