using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    [SerializeField] private PlayerControl player1;
    [SerializeField] private PlayerControl player2;
    [SerializeField] private BallControl ball;
    [SerializeField] private int maxScore;
    [SerializeField] private Trajectory trajectory;
    
    private Rigidbody2D m_player1Rb;
    private Rigidbody2D m_player2Rb;
    private Rigidbody2D m_ballRb;
    private CircleCollider2D m_ballCollider;
    private bool m_isDebugShown;

    public int MAXScore
    {
        get => maxScore;
        set => maxScore = value;
    }

    private void Awake()
    {
        m_player1Rb = player1.GetComponent<Rigidbody2D>();
        m_player2Rb = player2.GetComponent<Rigidbody2D>();
        m_ballRb = ball.GetComponent<Rigidbody2D>();
        m_ballCollider = ball.GetComponent<CircleCollider2D>();
    }
    

    private void OnGUI()
    {
        GUI.Label(new Rect(Screen.width / 2 - 150 - 12, 20, 100, 100), $"{player1.Score}");
        GUI.Label(new Rect(Screen.width / 2 + 150 - 12, 20, 100, 100), $"{player2.Score}");

        if (GUI.Button(new Rect(Screen.width / 2 - 60, 35, 120, 53), "RESTART"))
        {
            player1.ResetScore();
            player2.ResetScore();
            
            ball.SendMessage("RestartGame", .5f, SendMessageOptions.RequireReceiver);
        }

        if (player1.Score == maxScore)
        {
            GUI.Label(new Rect(Screen.width/2 -150, Screen.height/2 - 10, 2000, 1000), "PLAYER ONE WIN");
            ball.SendMessage("ResetBall", null, SendMessageOptions.RequireReceiver);
        }
        
        else if (player2.Score == maxScore)
        {
            GUI.Label(new Rect(Screen.width/2 +30, Screen.height/2 - 10, 2000, 1000), "PLAYER TWO WIN");
            ball.SendMessage("ResetBall", null, SendMessageOptions.RequireReceiver);
        }

        if (m_isDebugShown)
        {
            var oldColor = GUI.backgroundColor;
            GUI.backgroundColor = Color.red;

            var ballMass = m_ballRb.mass;
            var ballVelocity = m_ballRb.velocity;;
            var ballSpeed = ballVelocity.magnitude;
            var ballMomentum = ballMass * ballVelocity;
            var ballFriction = m_ballCollider.friction;

            var impPlayer1X = player1.LastContactPoint.normalImpulse;
            var impPlayer1Y = player1.LastContactPoint.tangentImpulse;
            var impPlayer2X = player2.LastContactPoint.normalImpulse;
            var impPlayer2Y = player2.LastContactPoint.tangentImpulse;

            var debugText = $"Ball mass = {ballMass}\n" +
                            $"Ball velocity = {ballVelocity}\n" +
                            $"Ball speed = {ballSpeed}\n" +
                            $"Ball momentum = {ballMomentum}\n" +
                            $"Ball friction = {ballFriction}\n" +
                            $"Last impulse from P1 = {impPlayer1X},{impPlayer1Y}\n" +
                            $"Last impulse from P2 = {impPlayer2X},{impPlayer2Y}\n";

            var guiStyle = new GUIStyle(GUI.skin.textArea) {alignment = TextAnchor.UpperCenter};
            GUI.TextArea(new Rect(Screen.width / 2 - 200, Screen.height - 200, 400, 110), debugText, guiStyle);
            GUI.backgroundColor = oldColor;
        }

        if (GUI.Button(new Rect(Screen.width / 2 - 60, Screen.height - 73, 120, 53), "TOGGLE\nDEBUG INFO"))
        {
            m_isDebugShown = !m_isDebugShown;
            trajectory.enabled = !trajectory.enabled;
        }
    }
}
