using System;
using System.Linq;
using UnityEngine;

public class Trajectory : MonoBehaviour
{
    [SerializeField] private BallControl ball;
    [SerializeField] private GameObject ballAtCollision;

    private CircleCollider2D m_ballCollider;
    private Rigidbody2D m_ballRb;

    private void Awake()
    {
        m_ballCollider = ball.GetComponent<CircleCollider2D>();
        m_ballRb = ball.GetComponent<Rigidbody2D>();
    }

    private void Update()
    {
        var drawBallAtCollision = false;
        var offsetHitPoint = new Vector2();
        var hit2Ds = new RaycastHit2D[10];
        if (Physics2D.CircleCastNonAlloc(m_ballRb.position, m_ballCollider.radius, m_ballRb.velocity.normalized, hit2Ds) > 0)
        {
            foreach (var hit2D in hit2Ds.Where(hit => hit.collider != null))
            {
                if (hit2D.collider.GetComponent<BallControl>() == null)
                {
                    var hitPoint = hit2D.point;
                    var hitNormal = hit2D.normal;
                    offsetHitPoint = hitPoint + hitNormal * m_ballCollider.radius;
                    DottedLine.DottedLine.Instance.DrawDottedLine(ball.transform.position, offsetHitPoint);
                    
                    if (hit2D.collider.GetComponent<SideWall>() == null)
                    {
                        var inVector = (offsetHitPoint - ball.TrajectoryOrigin).normalized;
                        var outVector = Vector2.Reflect(inVector, hitNormal);

                        var outDot = Vector2.Dot(outVector, hitNormal);
                        if (outDot > -1f && outDot < 1f)
                        {
                            DottedLine.DottedLine.Instance.DrawDottedLine(offsetHitPoint, offsetHitPoint + outVector * 10f);
                            drawBallAtCollision = true;

                        }
                    }
                    break;

                }
            }
        }
        
        if (drawBallAtCollision)
        {
            ballAtCollision.transform.position = offsetHitPoint;
            ballAtCollision.SetActive(true);
        }
        else
        {
            ballAtCollision.SetActive(false);
        }
      

     
    }
}
